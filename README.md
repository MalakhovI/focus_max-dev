# focusmax.top

## Run
 - Grunt should be install globally `npm install -g grunt-cli`.
 - run `npm i` in project folder.
 - run it:
    - `grunt run` - 'Home' page
    - `grunt run:cart` - 'Cart' page
    - `grunt run:checkout` - 'Checkout' page

 grunt run simple server for page.
 You can see something like this in console:

```
[BS] Access URLs:
 --------------------------------------
       Local: http://localhost:3060
    External: http://192.168.1.106:3060

```


 You can use IP from console (example: 192.168.1.106:3060) to connect your mobile device.
 
 Your mobile device should be in the same network which server.
